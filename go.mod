module gitlab.com/golib4/tonconsole-client

go 1.22.2

require gitlab.com/golib4/http-client v1.0.4

require (
	github.com/gorilla/schema v1.3.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/olivere/elastic/v7 v7.0.32 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/golib4/logger v1.0.6 // indirect
)
