package tonconsole

import (
	"errors"
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	return Client{
		httpClient: httpClient,
	}
}

type NftsResponse struct {
	Items []struct {
		Address string `json:"address"`
		Index   int64  `json:"index"`
		Sale    struct {
			Price struct {
				Value     string `json:"value"`
				TokenName string `json:"token_name"`
			} `json:"price"`
		} `json:"sale"`
		Collection struct {
			Address     string `json:"address"`
			Name        string `json:"name"`
			Description string `json:"description"`
		} `json:"collection"`
		Metadata struct {
			Name string `json:"name"`
		} `json:"metadata"`
		Previews []struct {
			Url        string `json:"url"`
			Resolution string `json:"resolution"`
		} `json:"previews"`
	} `json:"nft_items"`
}

var RateLimitError = errors.New("rate limiter error")

func (c Client) GetAccountNft(accountAddress string) (*NftsResponse, error) {
	var response NftsResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/accounts/%s/nfts", accountAddress),
		},
	}, &response)
	if httpError != nil {
		if httpError.Status == "429 Too Many Requests" {
			return nil, RateLimitError
		}
		return nil, fmt.Errorf("erro while trying to get nft in tonconsole: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to get nft in tonconsole: %s", err)
	}

	return &response, err
}

type AccountDataResponse struct {
	Address      string `json:"address"`
	Balance      uint64 `json:"balance"`
	LastActivity int64  `json:"last_activity"`
	Status       string `json:"status"`
}

func (c Client) GetAccountData(accountAddress string) (*AccountDataResponse, error) {
	var response AccountDataResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/accounts/%s", accountAddress),
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("erro while trying to get account data in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to get account data in tonconsole: %s", httpError)
	}

	return &response, err
}

type LastKnownMasterchainBlockResponse struct {
	WorkchainId int64  `json:"workchain_id"`
	Shard       string `json:"shard"`
	Seqno       uint64 `json:"seqno"`
}

type GetBlockTransactionsRequest struct {
	WorkchainId int64
	Shard       string
	Seqno       uint64
}

func (c Client) GetLastMasterchainBlock() (*LastKnownMasterchainBlockResponse, error) {
	var response LastKnownMasterchainBlockResponse

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: nil,
		Data: http.RequestData{
			Path: "/v2/blockchain/masterchain-head",
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying get last known masterchain block in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying get last known masterchain block in tonconsole: %s", httpError)
	}

	return &response, nil
}

type Transaction struct {
	Lt              uint64 `json:"lt"`
	Utime           uint64 `json:"utime"`
	Hash            string `json:"hash"`
	Success         bool   `json:"success"`
	TransactionType string `json:"transaction_type"`
	Account         struct {
		Address string `json:"address"`
	} `json:"account"`
	TotalFees uint64 `json:"total_fees"`
	OutMsgs   []struct {
		Value      uint64 `json:"value"`
		ForwardFee uint64 `json:"fwd_fee"`
		Source     struct {
			Address string `json:"address"`
		} `json:"source"`
		Destination struct {
			Address string `json:"address"`
		} `json:"destination"`
	} `json:"out_msgs"`
	InMsg *struct {
		Value       uint64 `json:"value"`
		Destination struct {
			Address string `json:"address"`
		} `json:"destination"`
		Source *struct {
			Address string `json:"address"`
		} `json:"source"`
		DecodedOpName *string `json:"decoded_op_name"`
	} `json:"in_msg"`
}

type BlockTransactionsResponse struct {
	Transactions []Transaction `json:"transactions"`
}

func (c Client) GetBlockTransactions(request GetBlockTransactionsRequest) (*BlockTransactionsResponse, error) {
	var response BlockTransactionsResponse

	blockId := fmt.Sprintf("(%d, %s, %d)", request.WorkchainId, request.Shard, request.Seqno)

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: nil,
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/blockchain/blocks/%s/transactions", blockId),
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying get block transactions in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying get block transactions in tonconsole: %s", httpError)
	}

	return &response, nil
}

type GetBlockRequest struct {
	WorkchainId int64
	Shard       string
	Seqno       uint64
}

type BlockResponse struct {
	TxQuantity uint64 `json:"tx_quantity"`
}

func (c Client) GetBlock(request GetBlockRequest) (*BlockResponse, error) {
	var response BlockResponse

	blockId := fmt.Sprintf("(%d, %s, %d)", request.WorkchainId, request.Shard, request.Seqno)

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: nil,
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/blockchain/blocks/%s", blockId),
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying get block transactions in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying get block transactions in tonconsole: %s", httpError)
	}

	return &response, nil
}

type AccountTransactionsRequest struct {
	AccountAddress string
	Query          AccountTransactionsQuery
}

type AccountTransactionsQuery struct {
	Limit int32 `schema:"limit"`
}

type AccountTransactionsResponse struct {
	Transactions []Transaction `json:"transactions"`
}

func (c Client) GetAccountTransactions(request AccountTransactionsRequest) (*AccountTransactionsResponse, error) {
	var response AccountTransactionsResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request.Query,
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/blockchain/accounts/%s/transactions", request.AccountAddress),
		},
	}, &response)

	if err != nil {
		return nil, fmt.Errorf("erro while trying to get account data in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to get account data in tonconsole: %s", httpError)
	}

	return &response, err
}

type GetAccountSeqnoRequest struct {
	AccountAddress string
}

type GetAccountSeqnoResponse struct {
	Seqno uint64 `json:"seqno"`
}

func (c Client) GetAccountSeqno(request GetAccountSeqnoRequest) (*GetAccountSeqnoResponse, error) {
	var response GetAccountSeqnoResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path: fmt.Sprintf("/v2/wallet/%s/seqno", request.AccountAddress),
		},
	}, &response)

	if err != nil {
		return nil, fmt.Errorf("erro while trying to get account seqno data in tonconsole: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to get account seqno data in tonconsole: %s", httpError)
	}

	return &response, err
}
